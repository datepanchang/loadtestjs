var express = require('express'),
  app = express(),
  http = require('http'),
  httpServer = http.Server(app);

app.use(express.static('.'));

app.get('/', function(req, res) {
  res.sendfile('./index.html');
});
app.listen(5000);
console.log('listening on port 5000');